init:
	docker-compose -f docker-compose.yml up -d redis postgresql api
	docker-compose run api bundle exec rake db:drop db:create db:migrate
	make start

start:
	docker-compose up -d

stop:
	docker-compose down

restart:
	make stop
	make start

test:
	docker-compose run api bundle exec rspec

remove:
	docker-compose down
	docker-compose rm -fsv

console:
	docker-compose run api rails c
