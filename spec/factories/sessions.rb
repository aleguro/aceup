FactoryBot.define do
  factory :session do
    coach_hash_id   "121212"
    client_hash_id  "212121"

    trait :ok do  
      association :ok
    end
  end

  factory :ok do
    start '2024-10-20 09:00'
    duration: 60 
  end
end