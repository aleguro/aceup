require 'rails_helper'

RSpec.describe "Sessions", type: :request do
  describe "POST /sessions" do
    context 'When a session creation is requested' do
      let(:session) { valid_session }
      let(:invalid_session_upper_bound) { upper_bound_invalid_session }
       
      it 'expect to successfully book a session when there is no overlapping' do

        post '/api/v1/sessions', params: session.to_json, headers: {
          'Request method'  => 'POST',
          'Accept'          => 'application/json',
          'content-type'    => 'application/json',
        }
    
        expect(response.code).to eq('201')
      end

      it 'expect to fails when the session overlaps with an upper bound' do

        post '/api/v1/sessions', params: session.to_json, headers: {
          'Request method'  => 'POST',
          'Accept'          => 'application/json',
          'content-type'    => 'application/json',
        }
    
        expect(response.code).to eq('201')

        post '/api/v1/sessions', params: invalid_session_upper_bound.to_json, headers: {
          'Request method'  => 'POST',
          'Accept'          => 'application/json',
          'content-type'    => 'application/json',
        }
    
        expect(response.code).to eq('422')
        expect(response.body).to match('The coach has another session at the same time')
      end
    end
  end
end
