def valid_session
  { "session": {  "coach_hash_id":"121212", 
                  "client_hash_id":"212121",
                  "start": "2023-10-20 09:00", 
                  "duration": 60 } }
end

def upper_bound_invalid_session
  { "session": {  "coach_hash_id":"121212", 
                  "client_hash_id":"212121",
                  "start": "2023-10-20 08:00", 
                  "duration": 61 } }
end