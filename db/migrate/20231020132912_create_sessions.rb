class CreateSessions < ActiveRecord::Migration[6.1]
  def change
    create_table :sessions do |t|
      t.string :coach_hash_id, index: true
      t.string :client_hash_id
      t.datetime :start
      t.integer :duration
      t.integer :day, index: true
      t.integer :from_in_minutes, index: true
      t.integer :to_in_minutes, index: true

      t.timestamps
    end
  end
end
