# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_10_20_132912) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "sessions", force: :cascade do |t|
    t.string "coach_hash_id"
    t.string "client_hash_id"
    t.datetime "start"
    t.integer "duration"
    t.integer "day"
    t.integer "from_in_minutes"
    t.integer "to_in_minutes"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["coach_hash_id"], name: "index_sessions_on_coach_hash_id"
    t.index ["day"], name: "index_sessions_on_day"
    t.index ["from_in_minutes"], name: "index_sessions_on_from_in_minutes"
    t.index ["to_in_minutes"], name: "index_sessions_on_to_in_minutes"
  end

end
