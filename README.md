# AceUp Tech Assessment - Backend

### System Requirements

- Docker
- Make

### Build first time

```
make init
```

### Run the app

```
make start
```

## Use the api from the command line

```
$ curl -X POST -H "Content-type: application/json" -H "Accept: application/json" localhost:3000/api/v1/sessions  -d '{"session": {"coach_hash_id":"121212", "client_hash_id":"212121","start": "2023-10-20 09:00", "duration": 59}}'
```

## Testing

```
make test
```

### Stop the app

```
make stop
```

### Un-install the api

```
make remove
```

### The solution 

 Since it was suggested to be creative, I decided to improve the overlapping logic and avoid performing date operations in the database.  

### The approach you used 

Instead of checking date ranges overlapping queries, I decided to make some transformations and produce some integer values in the table and also some indexes to boost the overlapping  

 Therefor a start date = 2024-06-01 9:00 AM  and a duration = 60 minutes

 it is translated to 

  - day: 20231020
  - from_in_minutes: 900
  - to_in_minutes:  1000

### Any challenges faced 

I would have wanted in case of having more time to benchmark my soltuion and compare it with a simpler approach

### Potential improvements

- Add limit of sessions per day any given coach can perform on daily basis
- Improve testing by drying off session requests, adding models specs, adding coverage
- Cache sessions in memory for top wanted coaches and speed up validations
- Add rubocop and brakeman for better code control
- Make a deploy pipeline to see the api live
