module Overlapable
  extend ActiveSupport::Concern

  included do
    validate    :sessions_dont_overlap
    before_save :compute_session
  end

  ## Validates two sessions don't overlap for the same coach
  ##   Existing records
  ##  |-----------------|-----------------|-----------------|
  ##  | coach_hash_id   | start           | duration        |
  ##  |-----------------|-----------------|-----------------| 
  ##  | 123456          | 2023-10-20 10:00| 60              |
  ##  |-----------------|-----------------|-----------------|
  ##  
  ##  New session for coach_hash_id: 123456 from 2023-10-20 09:00 to 2023-10-20 10:01 should be invalid
  ##  New session for coach_hash_id: 123456 from 2023-10-20 09:00 to 2023-10-20 10:00 should be valid
  def sessions_dont_overlap

    return unless Session.where(coach_hash_id: coach_hash_id)
                          .where(day: start.to_datetime.flat_day)
                          .in_range(from_in_flat_minutes..to_in_flat_minutes)
                          .exists?

    errors.add(:coach_hash_id, 'The coach has another session at the same time')
  end

  ## Compute session start and duration to a integer expression to improve overlap validation performance
  ## Translates start and duration from
  ##   2023-10-20 10:00 | 60 
  ##    ->
  ##      day:  20231020
  ##      from: 1000
  #       to:   1100
  def compute_session

    self.day = start
                  .to_datetime
                  .flat_day

    self.from_in_minutes = from_in_flat_minutes
    self.to_in_minutes   = to_in_flat_minutes
  end

  private

  def from_in_flat_minutes
    start
      .to_datetime
      .flat_time
  end

  def to_in_flat_minutes
    ( start + duration.minutes )
      .to_datetime
      .flat_time
  end
end