class Session < ApplicationRecord
  include Overlapable

  ## Validations
  validates :coach_hash_id, :client_hash_id, :start, :duration, presence: true
  validates :duration, numericality: { only_integer: true, greater_than: 0 } 
  validate  :check_coach_and_client

  ## Validates the coach and the client are different
  def check_coach_and_client

    return if coach_hash_id != client_hash_id

    errors.add(:coach_hash_id, 'The coach and the client have to be different!') 
  end

  ## Scopes
  scope :in_range, ->(range) { where( "to_in_minutes >= ?", range.first ).where( "from_in_minutes <= ?", range.last ) }
end
