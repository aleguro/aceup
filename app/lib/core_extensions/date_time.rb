module CoreExtensions
  module DateTime
    def flat_day
      to_s.gsub('-','').to_i
    end

    def flat_time
      strftime('%H%M').to_i
    end 
  end
end