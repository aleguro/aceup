module CoreExtensions
  module Time
    def flat    
      "#{self.hour}#{self.min}".to_i
    end
  end
end