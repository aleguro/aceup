module Api
  module V1
    class SessionsController < ApplicationController

      # POST /sessions
      #  { "coach_hash_id": "123456",
      #     "client_hash_id": "654321",
      #     "start": "2023-10-20 10:00",
      #     "duration": 60 }
      def create

        session = Session.new(session_params)

        if session.valid? && session.save

          render json: { session: session }, status: :created
        else

          render json: { errors: session.errors.map(&:message) }, status: :unprocessable_entity
        end
      end

      private

      def session_params
        params
          .require(:session)
          .permit(:coach_hash_id, :client_hash_id, :start, :duration)
      end
    end
  end
end